// import axios from "axios";
// import JSONBIG from "json-bigint"


// //解决处理19位id精度失效问题
// axios.defaults.transformResponse = [
//     function (data) {
//     const json = JSONBIG({
//     storeAsString: true
//     })
//     const res = json.parse(data)
//     return res
//     }
// ]

// //1:利用axios对象的方法create, 去创建一个axios实例
// //2:request就是axios，只不过稍微配置一下
// const req = axios.create({
//     //代表请求超时的时间5s
//     timeout:5000,
//     headers:{
//         "Content-Type":"application/json;charset=utf-8"
//     }
// });
// //请求拦截器:在发请求之前，请求拦截器可以检测到，可以在请求发出去之前做一些事情
// req.interceptors.request.use((config)=>{
//     config.headers = config.headers || {}
//     //从localStorage取出token
//     if(localStorage.getItem('token')){
//         //把token 取出来写入请求头
//         config.headers.token = localStorage.getItem('token') || ""
//     }
//     return config;
// })


// //响应拦截
// req.interceptors.response.use((res)=>{
//     //进度条结束
//     const code = res.data.status;
//     if(code != 200){
//         return Promise.reject(res.data)
//     }
//     return res.data;
// },
// (err)=>{
//     console.log(err);
// }
// )


// export default req;