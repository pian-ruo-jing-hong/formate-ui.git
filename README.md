# format-ui

## 项目 下载依赖包

```
npm i element-ui -S  
npm install --save vue-typed-js
npm install npm install axios
```

## 项目运行命令

```
npm run serve
```

## 项目描述

网易云 音乐 .NCM 转换 MP3类型

## 引用说明

流光按钮样式主要参考

> https://www.bilibili.com/video/BV1cK4y1k7qG/?spm_id_from=333.337.search-card.all.click&vd_source=12817c5c11655a9fe55a3c50e856661e

## 项目运行效果图

![1691845300451](image/README/1691845300451.png)
