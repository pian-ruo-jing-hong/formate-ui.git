const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave : false,
  //开启代理服务器
  devServer:{
    proxy:{
      //路径/user开头走代理
      '/home/upload/music' : {
        target: 'http://localhost:8081',
        ws:true,//支持websocket
        changeOrigin:true //用于控制请求头中的host值
      },
      '/home' : {
        target: 'http://localhost:8081',
        ws:true,//支持websocket
        changeOrigin:true //用于控制请求头中的host值
      }
    }
  },
  publicPath: '/static/', 
})
